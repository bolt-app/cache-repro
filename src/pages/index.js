import React from 'react';
import { graphql } from 'gatsby';

export default ({ data }) => {
  return <pre>{JSON.stringify(data, null, 2)}</pre>;
};

export const query = graphql`
  query HomePage {
    allWpPost {
      nodes {
        title
      }
    }
  }
`;
