module.exports = {
  plugins: [
    {
      resolve: `@bolt-app/gatsby-source-wordpress-experimental`,
      options: {
        url: `https://dev-wa-bolt.pantheonsite.io/graphql`,
        verbose: true,
        debug: {
          graphql: {
            showQueryOnError: true,
            showQueryVarsOnError: true,
            copyQueryOnError: true,
            panicOnError: true,
          },
        },
        excludeFields: [`attributes`],
        develop: {
          nodeUpdateInterval: 86400,
        },
        type: {
          MediaItem: {
            lazyNodes: true,
          },
          Plugin: {
            exclude: true,
          },
          UserRole: {
            exclude: true,
          },
          Theme: {
            exclude: true,
          },
          Comment: {
            exclude: true,
          },
        },
      },
    },
  ],
};
